from datetime import datetime
import  csv, re, os
from os.path import exists
from datetime import timedelta, datetime
from dateutil.tz import tzutc, tzlocal
from dateutil import parser

from uart import SerialWrapper


def reboot_system():
    #serial = SerialWrapper()
    #serial.sendData(data)
    os.system("shutdown /r /t 1")

PERIFERIC_GAS = [
    '109',
    '110',
    '130',
    '252',
    '262',
]
'''
Contenido del archivo de origen
fichero FX
1 código identificador al inicio (formado por 3 dígitos)
60 valores, es decir un valor cada 1 minuto
Para seleccionar los valores es preferible contar como separador las unidades
(carácter alfanumérico) e incluirlo en el anterior dato númerico, finalmente
realizar un strip() para eliminar espacios.
'''
def split_on_first_alpha(i):
    splitted_numbers = re.split('([a-zA-Z])', i)

    numbers = []
    while len(splitted_numbers) > 1:
        number = splitted_numbers.pop(0).strip()
        unit = splitted_numbers.pop(0)
        numbers.append(f'{number}{unit}')
    return numbers


def decode_origin_file(line):
    code = line[:3]
    data = split_on_first_alpha(line[3:])
    return (code, data)

'''
Ejemplo fileinfo:
{
    '252': [
        '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V'
    ],
    '261': [
        '22.4R', '22.5V', '22.3V', '22.6N', '22.6V', '22.9V', '22.6A', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V'
    ]
}

'''
def get_data_from_path(path, config):
    file_exists = exists(path)
    fileinfo = {}
    no_data_key = '0000N'
    no_data_code = []
    count_code = 0
    if file_exists:
        with open(path) as file:
            for line in file:
                count_code += 1
                code, data = decode_origin_file(line)
                fileinfo[code] = data
                if data == [no_data_key] * 60:
                    no_data_code.append(code)
        if len(no_data_code) == count_code:
            force_data_reboot = parser.parse(config.get('force_data_reboot')) if config.get('force_data_reboot') else None
            if not force_data_reboot:
                config.set('force_data_reboot', str(get_utc_actual()))
            elif (get_utc_actual() - force_data_reboot).total_seconds() >= 3600:
                config.set('force_date_reboot', False)
                config.set('force_data_reboot', False)
                reboot_system()
        else:
            config.set('force_date_reboot', False)
            config.set('force_data_reboot', False)

    else:
        force_date_reboot = parser.parse(config.get('force_date_reboot')) if config.get('force_date_reboot') else None
        if not force_date_reboot:
            config.set('force_date_reboot', str(get_utc_actual()))
        return {}

    return fileinfo

def get_list_gas(path, config):
    info = get_data_from_path(path, config)
    return list(info.keys())

'''
Input:
Los datos de origen se encuentran en el directorio: C:\Rdiari\FX\@Año\@Mes.
Ejemplo: C:\Rdiari\FX\22\01
El nombre del fichero esta compuesto por las letras FX
La fecha actual expresada como día/mes/año
En la extensión se añade .MX, en donde la X hace referencia a la hora actual
Ejemplo: M00 a M23
Ejemplo final nombre archivo: FX110122.M13
Ruta definitiva ejemplo: C:\Rdiari\FX\22\01\FX110122.M13
'''


def fx_filepath_date(currentYear, currentMonth, currentDay, currentHour):
    origin_path = f'C:\Rdiari\FX\{currentYear}\{currentMonth}'
    filename = f'FX{currentDay}{currentMonth}{currentYear}.M{currentHour}'
    return f'{origin_path}\{filename}'

def fx_filepath_time(currentHour, actualdate=None):
    if actualdate:
        now = datetime.fromisoformat(actualdate)
        currentHour = str(now.hour).zfill(2)
        currentYear = str(now.year)[2:]
        currentMonth = str(now.month).zfill(2)
        currentDay = str(now.day).zfill(2)
        origin_path = f'fx_test/{currentYear}/{currentMonth}'
        filename = f'FX{currentDay}{currentMonth}{currentYear}.M{currentHour}'
        print(f'{origin_path}/{filename}')
        return f'{origin_path}/{filename}'
    else:
        if not isinstance(currentHour, str):
            currentHour = str(currentHour).zfill(2)
        currentYear = str(datetime.now().year)[2:]
        currentMonth = str(datetime.now().month).zfill(2)
        currentDay = str(datetime.now().day).zfill(2)
        origin_path = f'fx_test\{currentYear}\{currentMonth}'
        filename = f'FX{currentDay}{currentMonth}{currentYear}.M{currentHour}'
        return fx_filepath_date(currentYear, currentMonth, currentDay, currentHour)

def fx_filepath(actualdate=None):
    currentHour = str(datetime.now().hour).zfill(2)
    return fx_filepath_time(currentHour, actualdate)

def LIST_GAS(config, actualdate):
    return get_list_gas(fx_filepath(actualdate), config)


'''
Output:
Se almacenará en dos ficheros CSV denominados DPN y MCT
Directorio C:\@año\@mes
Ejemplo: C:\2022\01
Definición nombre archivo
1. Indica si contiene datos DPN (de primer nivel) o MCT (medianas de corto plazo).
2. Campo identificador del establecimiento, formado por 5 números, se incluyen los ceros a la izquierda.
3. Guion bajo.
4. Fecha en formato número del año completo, mes y día sin separación.
5. Extensión del archivo CSV.

Ejemplo DPN: DPN00001_20220111.csv
Ejemplo MCT: MCT00001_20220111.csv
'''

def DPN_filepath(codiemis, actual_date=None):
    if actual_date:
        now = datetime.fromisoformat(actual_date)
        currentYear = str(now.year)
        currentMonth = str(now.month).zfill(2)
        currentDay = str(now.day).zfill(2)
        currentHour = str(now.hour).zfill(2)
        folder = f"{currentYear}/{currentMonth}"
        os.makedirs(folder, exist_ok=True)
        filepath = f"{folder}/DPN{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
        return filepath
    else:
        currentYear = str(datetime.now().year)
        currentMonth = str(datetime.now().month).zfill(2)
        currentDay = str(datetime.now().day).zfill(2)
        currentHour = str(datetime.now().hour).zfill(2)
        folder = f"C:\DadesXEAC\{currentYear}\{currentMonth}"
        os.makedirs(folder, exist_ok=True)
        filepath = f"{folder}\DPN{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
        return filepath

def MCT_filepath(codiemis, actual_date=None):
    if actual_date:
        now = datetime.fromisoformat(actual_date)
        currentYear = str(now.year)
        currentMonth = str(now.month).zfill(2)
        currentDay = str(now.day).zfill(2)
        currentHour = str(now.hour).zfill(2)
        folder = f"{currentYear}/{currentMonth}"
        os.makedirs(folder, exist_ok=True)
        filepath = f"{folder}/MCT{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
        return filepath
    else:
        currentYear = str(datetime.now().year)
        currentMonth = str(datetime.now().month).zfill(2)
        currentDay = str(datetime.now().day).zfill(2)
        currentHour = str(datetime.now().hour).zfill(2)
        folder = f"C:\DadesXEAC\{currentYear}\{currentMonth}"
        os.makedirs(folder, exist_ok=True)
        filepath = f"{folder}\MCT{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
        return filepath

'''
Example info:
{
  '252_NSerie': '',
  '252_FC(c)x2': '1',
  '252_FC(b)x': '',
  '252_FC(a)': '',
  '252_Ival': '',
  '252_IDUnitat': '',
  '261_NSerie': '1',
  '261_FC(c)x2': '',
  '261_FC(b)x': '',
  '261_FC(a)': '',
  '261_Ival': '',
  '261_IDUnitat': '',
  '252_NumMin': '60',
  '252_F_VLE': '',
  '252_V_IMP': '',
  '252_F_IMP': '',
  '252_V_INV': '',
  '252_F_INV': '',
  '252_V_PRT': '',
  '252_F_PRT': '',
  '252_V_GEH': '',
  '252_F_GEH': '',
  '261_NumMin': '10',
  '261_F_VLE': '',
  '261_V_IMP': '',
  '261_F_IMP': '',
  '261_V_INV': '',
  '261_F_INV': '',
  '261_V_PRT': '',
  '261_F_PRT': '',
  '261_V_GEH': '',
  '261_F_GEH': '',
  'CodiEmis': '123',
  'IDFocus': '2',
  'IDLegis': '1',
  'IDSubmode': '3'
}
'''

def parseMinutesToText(minutes):
    hours = minutes // 60
    minutes = minutes % 60
    return "{}h {}m".format(hours, minutes)

def parseValueNULL(value):
    if not value:
        return 'NULL'
    return value

def parseValueNN(value):
    if not value:
        return 'NN'
    return value

def parseExponentFloat(value):
    if not value or not float(value): value = 0
    return "{:.6e}".format(float(value)).upper()

def set_info_DPN(info, code, marca_temps, config, generate=False):
    headers = ["CodiEmis","IDFocus","MarcaTemps","IDParametre","NSerie","FC(c)x2","FC(b)x","FC(a)","Ival","IDLegis","IDSubmode","Valor","CV","IDUnitat"]

    codiemis = info['CodiEmis'].zfill(5)
    idfocus = info['IDFocus']
    marcatemps = get_plain_datetime_actual(marca_temps)
    nserie = info[f"{code}_NSerie"]
    fccx2 = parseExponentFloat(info[f"{code}_FC(c)x2"])
    fcbx = parseExponentFloat(info[f"{code}_FC(b)x"])
    fca = parseExponentFloat(info[f"{code}_FC(a)"])
    ival = parseExponentFloat(info[f"{code}_Ival"])
    idlegis = info['IDLegis']
    idsubmode = info['IDSubmode']
    print('info', info)
    if generate:
        fx_data = get_data_from_path(fx_filepath(marca_temps.isoformat()), config)
    else:
        fx_data = get_data_from_path(fx_filepath(), config)
    if code in fx_data:
        pos = marca_temps.minute
        if len(fx_data[code]) >= pos:
            valor = fx_data[code][pos][:-1]
            cv = fx_data[code][pos][-1]
            if code == '252' and cv == 'S':
                count = config.get(f'Acumulat_Hores_Substitucio')
                if not count: count = 0
                else: count = float(count)
                count += 1 / 60
                config.set(f'Acumulat_Hores_Substitucio', str(count))
        else:
            valor = 'NULL'
            cv = 'NULL'
    else:
        return False
    idunitat = info[f"{code}_IDUnitat"]

    if generate:
        filepath = DPN_filepath(codiemis, marca_temps.isoformat())
    else:
        filepath = DPN_filepath(codiemis)

    data = [
        parseValueNULL(codiemis),
        parseValueNULL(idfocus),
        parseValueNULL(marcatemps),
        parseValueNULL(code),
        parseValueNULL(nserie),
        parseExponentFloat(fccx2),
        parseExponentFloat(fcbx),
        parseExponentFloat(fca),
        parseExponentFloat(ival),
        parseValueNULL(idlegis),
        parseValueNULL(idsubmode),
        parseExponentFloat(valor),
        parseValueNULL(cv),
        parseValueNULL(idunitat),
    ]
    write_file(filepath, headers, data)

def is_report_values(values):
    count = 0
    for value in values:
        if value.endswith('C') or value.endswith('N') or value.endswith('V'): count += 1
    return (count / len(values)) >= (2 / 3)

def calc_vinc(config, values, c6, c7, c8, cabal, code):
    success_values = []
    for value in values:
        if value.endswith('V') or value.endswith('A'):
            success_values.append(float(value[:-1]))
    if success_values:
        x = sum(success_values) / len(success_values)
        calibration = calc_calibration(x, c6, c7, c8)
        result = parseExponentFloat(calibration * cabal)
    else:
        result = 'NULL'
    return result

def calc_calibration(x, c6, c7, c8):
    return c6*x*x+c7*x+c8

def calc_finv(config, values, code):
    count = 0
    for value in values:
        if value.endswith('A') or value.endswith('V'): count += 1
    if (count / len(values)) >= (2 / 3): result = 'RS'
    else: result = 'NN'
    return result

def set_info_MCT(config, code, marca_temps, periode, llindar_no_afectat, generate=False):
    info = config.as_dict()
    headers = ["CodiEmis","IDFocus","MarcaTemps","NumMinuts","IDParametre","IDLegis","IDSubmode","V_VLE", "F_VLE","V_IMP", "F_IMP","V_INV", "F_INV","V_PRT", "F_PRT", "V_GEH","F_GEH"]

    codiemis = info['CodiEmis'].zfill(5)
    idfocus = info['IDFocus']
    marcatemps = get_plain_datetime_actual(marca_temps)
    numminuts = info[f"{code}_NumMin"]
    idlegis = info['IDLegis']
    idsubmode = info['IDSubmode']
    cabal = float(info['Cabal']) if info['Cabal'] else 0
    vimp = 'NULL'
    fimp = info[f"{code}_F_IMP"]
    #vinv = parseExponentFloat(info[f"{code}_V_INV"])
    #finv = info[f"{code}_F_INV"]
    #vprt = parseExponentFloat(info[f"{code}_V_PRT"])
    #fprt = info[f"{code}_F_PRT"]
    vgeh = parseExponentFloat(info[f"{code}_V_GEH"])
    fgeh = info[f"{code}_F_GEH"]
    llindaranomal = float(info[f"{code}_Llindar_Anomal"]) if info[f"{code}_Llindar_Anomal"] else 0
    nmitjaneslla = float(info["N_Mitjanes_LLA"]) if info["N_Mitjanes_LLA"] else 0
    if generate:
        fx_data = get_data_from_path(fx_filepath(marca_temps.isoformat()), config)
    else:
        fx_data = get_data_from_path(fx_filepath(), config)
    if generate:
        fx_pasthour_data = get_data_from_path(fx_filepath((marca_temps - timedelta(hours=1)).isoformat()), config)
    else:
        fx_pasthour_data = get_data_from_path(fx_filepath_time((marca_temps - timedelta(hours=1)).hour), config)
    actualminute = marca_temps.minute

    print('fx_data', fx_data)
    if actualminute == 0 and code in fx_pasthour_data:
        values = fx_pasthour_data[code][(periode * -1):]
    elif actualminute - periode  >= 0 and code in fx_data:
        values = fx_data[code][actualminute - periode:actualminute]
    else:
        actualvalues = fx_data[code][0:actualminute]
        if code in fx_pasthour_data:
            pastvalues = fx_pasthour_data[code][(actualminute - periode):]
            values = actualvalues + pastvalues
        else:
            values = actualvalues

    success_values = []
    for value in values:
        if value.endswith('V'): success_values.append(float(value[:-1]))

    print(values)
    report = is_report_values(values)

    fvle = None
    vvle = None
    c6 = float(info[f"{code}_FC(c)x2"]) if info[f"{code}_FC(c)x2"] else 0
    c7 = float(info[f"{code}_FC(b)x"]) if info[f"{code}_FC(b)x"] else 0
    c8 = float(info[f"{code}_FC(a)"]) if info[f"{code}_FC(a)"] else 0


    if periode == 10:
        vinv = 'NULL'
        finv = 'NN'
        vprt = 'NULL'
        fprt = 'NN'
    else:
        vinv = calc_vinc(config, values, c6, c7, c8, cabal, code)
        finv = calc_finv(config, values, code)
        vprt = vinv
        fprt = finv
    acumulat_llindar = int(info["Acumulat_Llindar"])
    acumulat_mitjanes = int(info["Acumulat_Mitjanes_LLA"])
    ic = float(info[f"{code}_IC"]) if info[f"{code}_IC"] else 0
    if success_values:
        median = sum(success_values) / len(success_values)
        x = float(median)
        calibration = calc_calibration(x, c6, c7, c8)
        if calibration < float(llindaranomal):
            vvle = calibration - calibration * ic
        else:
            vvle = calibration - float(llindaranomal) * ic
        if vvle < 0: vvle = 0

        if report:
            if (len(success_values) / len(values)) >= (2 / 3):
                config.set(f"{code}_Acumulat_Indisponibilitat", str(0))
                config.set(f"{code}_Indisponibilitat", str(0))
                if median > float(llindaranomal):
                    if acumulat_llindar >= float(nmitjaneslla):
                        if llindar_no_afectat:
                            llindar_no_afectat = False
                            acumulat_mitjanes += int(numminuts)
                            config.set("Acumulat_Mitjanes_LLA", str(acumulat_mitjanes))
                            config.set("text_Acumulat_Mitjanes_LLA", parseMinutesToText(acumulat_mitjanes))
                        fvle = 'VA'
                    else:
                        if llindar_no_afectat:
                            llindar_no_afectat = False
                            acumulat_llindar += int(numminuts)
                            config.set("Acumulat_Llindar", str(acumulat_llindar))
                            config.set("text_Acumulat_Llindar", parseMinutesToText(acumulat_llindar))
                        fvle = 'VD'
            else:
                indisponibilitat = float(info[f"{code}_Indisponibilitat"]) if f"{code}_Indisponibilitat" in info and info[f"{code}_Indisponibilitat"] else 0
                acumulat_indisponibilitat = int(info[f"{code}_Acumulat_Indisponibilitat"]) if f"{code}_Acumulat_Indisponibilitat" in info and info[f"{code}_Acumulat_Indisponibilitat"] else 0
                if indisponibilitat > (6 * 60):
                    config.set(f"{code}_Indisponibilitat", str(0))
                    acumulat_indisponibilitat += 1
                    config.set(f"{code}_Acumulat_Indisponibilitat", str(acumulat_indisponibilitat))
                else:
                    indisponibilitat += periode
                    config.set(f"{code}_Indisponibilitat", str(indisponibilitat))
                fvle = 'RN'
        else:
            fvle = 'NN'
    else:
        median = 0
        success_rate = 0
    if generate:
        filepath = MCT_filepath(codiemis, marca_temps.isoformat())
    else:
        filepath = MCT_filepath(codiemis)
    data = [
        parseValueNULL(codiemis),
        parseValueNULL(idfocus),
        parseValueNULL(marcatemps),
        parseValueNULL(numminuts),
        parseValueNULL(code),
        parseValueNULL(idlegis),
        parseValueNULL(idsubmode),
        parseExponentFloat(vvle),
        parseValueNN(fvle),
        parseValueNULL(vimp),
        parseValueNN(fimp),
        parseValueNULL(vinv),
        parseValueNN(finv),
        parseValueNULL(vprt),
        parseValueNN(fprt),
        parseValueNULL(vgeh),
        parseValueNN(fgeh),
    ]
    write_file(filepath, headers, data)
    return llindar_no_afectat

def is_last_time_done(filepath, actual_utc):
    data = read_file_last_line(filepath)
    if data:
        marca_temps = data.split(';')[2]
        return get_datetime_from_plain(marca_temps) == actual_utc
    return False

def read_file_last_line(filepath):
    file_exists = exists(filepath)
    if file_exists:
        csvfile = open(filepath)
        lastline = csvfile.readlines()[-1]
        csvfile.close()
        return lastline
    return None

def write_file(filepath, headers, data):
    file_exists = exists(filepath)
    csvfile = open(filepath, 'a', newline='')
    csvwriter = csv.writer(csvfile, delimiter=';')
    if not file_exists: csvwriter.writerow(headers)
    csvwriter.writerow(data)
    del csvwriter
    csvfile.close()

def get_plain_datetime_actual(get_utc_actual):
    return str(get_utc_actual).replace('+',' +')

def get_utc_actual():
    utc = datetime.now(tzutc())
    local = utc.astimezone(tzlocal())
    return local - timedelta(seconds=local.second, microseconds=local.microsecond)

def get_datetime_from_plain(plain_date):
    return datetime.strptime(plain_date, '%Y-%m-%d %H:%M:%S %z')

'''
This methods is executed every minute called by the interface
'''
def set_info_DPN_MCT(config, actualdate):
    info = config.as_dict()
    result = False
    actual_marca_temps = get_utc_actual()
    llindar_no_afectat = True
    for code in LIST_GAS(config):
        if code not in PERIFERIC_GAS and actualdate != info['initDate'] and abs(int(info['initMinute'])- actual_marca_temps.minute) % int(info[f"{code}_NumMin"]) == 0:
            llindar_no_afectat = set_info_MCT(config, code, actual_marca_temps, int(info[f"{code}_NumMin"]), llindar_no_afectat)
    if not is_last_time_done(DPN_filepath(info['CodiEmis'].zfill(5)), actual_marca_temps):
        for code in LIST_GAS(config):
            result = set_info_DPN(info, code, actual_marca_temps, config)

    return result


def generate_info_DPN_MCT(config, actualdate):
    info = config.as_dict()
    result = False
    actual_date = datetime.fromisoformat(actualdate).replace(tzinfo=tzutc())
    local = actual_date.astimezone(tzlocal())
    actual_marca_temps = local - timedelta(seconds=local.second, microseconds=local.microsecond)
    print('actual_marca_temps', actual_marca_temps)
    llindar_no_afectat = True
    for code in LIST_GAS(config, actualdate):
        if code not in PERIFERIC_GAS and actualdate != info['initDate'] and abs(int(info['initMinute'])- actual_marca_temps.minute) % int(info[f"{code}_NumMin"]) == 0:
            llindar_no_afectat = set_info_MCT(config, code, actual_marca_temps, int(info[f"{code}_NumMin"]), llindar_no_afectat, True)
    for code in LIST_GAS(config, actualdate):
        result = set_info_DPN(info, code, actual_marca_temps, config, True)

    return result
