from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QDesktopWidget,
    QSystemTrayIcon,
    QMenu,
    QAction,
)
from PyQt5.QtGui import (
    QPixmap,
    QIcon,
)

from PyQt5.QtCore import (
    pyqtSlot,
)

from pyqtconfig import ConfigManager
from datetime import datetime, timedelta
import sys, os
from adcData import generate_info_DPN_MCT
from threading import Thread


# DATA INICIAL I FINAL PER GENERAR ELS FITXERS DPN I MCT
data_inicial = datetime(2022, 1, 1)
data_final = datetime(2022, 2, 28, 23, 59, 0)

gasDict = {
    '109': 'Pressión',
    '110': 'Temperatura',
    '112': 'Partículas',
    '130': 'Humedad H2O(v)',
    '142': 'Hg',
    '212': 'SO2',
    '220': 'NOx',
    '221': 'NO',
    '223': 'NH3',
    '224': 'NH3 net',
    '234': 'HCI',
    '241': 'NO2',
    '252': 'O2',
    '261': 'CO',
    '261': 'CO2',
    '270': 'COT',
    '280': 'HF',
    '349': 'Caudal a depurar*2',
    '350': 'Cauda de gases emitidos*3',
    '352': 'Potencia',
    '353': 'Tipo de combustible',
    '354': 'Potencia de turbina',
    '362': 'Temperatura de la camara 1',
}

global_stats = {
    'CodiEmis': '528',
    'IDFocus': 'SC',
    'IDLegis': '5',
    'IDSubmode': '1',
    'Cabal': '50',
    'N_Mitjanes_LLA': '7200',
    'Acumulat_Llindar': 0,
    'Acumulat_Mitjanes_LLA': 0,
    '252_NSerie': 'N1-T8-0462',
    '252_FC(c)x2': 0,
    '252_FC(b)x': 1,
    '252_FC(a)': 0,
    '252_Ival': 0,
    '252_IDUnitat': '11',
    '252_IC': '',
    '252_Hores_Substitucio': 10,
    '252_NumMin': 10,
    '252_V_IMP': '',
    '252_F_IMP': 'NN',
    '252_V_GEH': '',
    '252_F_GEH': 'NN',
    '252_Llindar_Anomal': 5,
    '261_NSerie': 'N1-T8-0462',
    '261_FC(c)x2': 0,
    '261_FC(b)x': 0.98,
    '261_FC(a)': 3.05,
    '261_Ival': 0,
    '261_IDUnitat': '09',
    '261_IC': 0.1,
    '261_NumMin': 30,
    '261_V_IMP': '',
    '261_F_IMP': 'NN',
    '261_V_GEH': '',
    '261_F_GEH': 'NN',
    '261_Llindar_Anomal': 100,
}

def resource_path(relative_path):
     if hasattr(sys, '_MEIPASS'):
         return os.path.join(sys._MEIPASS, relative_path)
     return os.path.join(os.path.abspath("."), relative_path)

class Logo(QWidget):
    def __init__(self):
        super().__init__()

        # creating label
        self.label = QLabel(self)

        # loading image
        self.pixmap = QPixmap(resource_path('utidlogo.png'))

        # adding image to label
        self.label.setPixmap(self.pixmap)

        # Optional, resize label to image size
        self.label.resize(self.pixmap.width(), self.pixmap.height())

class MainWindow(QMainWindow):
    def __init__(self, app):
        super(MainWindow, self).__init__()
        self.app = app
        self.config = ConfigManager()
        defaults = {}
        n = datetime.now()
        defaults['initMinute'] = str(0)
        defaults['initDate'] = datetime(2022, 1, 1).isoformat()
        defaults['Acumulat_Mitjanes_LLA_actual_year'] = str(2022)
        defaults['Acumulat_Mitjanes_LLA'] = '0'
        defaults['Acumulat_Llindar'] = '0'
        self.config.set_defaults({**defaults, **global_stats})

        self.send_thread()

        layout = QVBoxLayout()
        layout1 = QHBoxLayout()
        layout1.setContentsMargins(0,0,0,0)
        layout1.setSpacing(0)

        layout1.addWidget(Logo())

        layout.addLayout( layout1 )

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

    def initUI(self):
        self.setWindowTitle('UTID Innovation and Development Technical Unit')
        self.resize(600, 600)
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    @pyqtSlot()
    def send_thread(self):
        t = Thread(target = self.working)
        t.start()

    def working(self):
        last_datetime_read = data_inicial
        while last_datetime_read <= data_final:
            generate_info_DPN_MCT(self.config, last_datetime_read.isoformat())
            last_datetime_read += timedelta(minutes=1)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    window = MainWindow(app)
    icon = QIcon(resource_path("icon.png"))
    tray = QSystemTrayIcon()
    tray.setIcon(icon)
    tray.setVisible(True)
    menu = QMenu()
    option0 = QAction("Obrir")
    option0.triggered.connect(window.show)
    menu.addAction(option0)
    quit = QAction("Tancar")
    quit.triggered.connect(app.quit)
    menu.addAction(quit)
    tray.setContextMenu(menu)
    window.initUI()
    app.setWindowIcon(QIcon(resource_path('icon.png')))
    sys.exit(app.exec_())
