import serial

## PORT WINDOWS COM3
# PORT LINUX /dev/ttyUSB0
class SerialWrapper:
    def __init__(self, port='COM3',  baudrate= 115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE):
        self.ser = serial.Serial(
            port=port,
            baudrate=baudrate,
            bytesize=bytesize,
            parity=parity,
            stopbits=stopbits,
            timeout=None,
            xonxoff=False,
            rtscts=False,
            write_timeout=None,
            dsrdtr=False,
            inter_byte_timeout=None,
            exclusive=None
        )

    def sendData(self, data):
        data += "\r\n"
        self.ser.write(data.encode())
